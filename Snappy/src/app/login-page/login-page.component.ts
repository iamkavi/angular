import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() public appName: string;
  credentials=new User('','');
  login(){
    alert("Hi " + this.credentials.username + ". You are Succesfully logged in!");
  }
  @Output() existingUser = new EventEmitter();
  setNewUser(){
    this.existingUser.emit(false);
  }
}
