import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appName = 'Snappy';
  existingUser=true;
  setExistingUser(event:boolean){
    this.existingUser=event;
  }
}
