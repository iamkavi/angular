import { AbstractControl } from "@angular/forms";

export function passwordMatchingValidator(control: AbstractControl) { 
    console.log(control);
    const password = control.get("pazzword").value !=''?control.get("pazzword"):null; 
    const confirmPassword = control.get("confirmPazzword").value !=''?control.get("confirmPazzword"):null; 
    return password && confirmPassword && password.value !== confirmPassword.value ? { 'mismatch': true } : null; 
}