import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { paramPasswordValidator } from './param-password.validator';
import { passwordMatchingValidator } from './password-matching.validator';
import { passwordValidator } from './password.validator';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  constructor(private readonly formBuilder: FormBuilder) { }

  passwordPattern =  new RegExp('[A-Za-z0-9]{6,}');
  
  newUserForm =  this.formBuilder.group({
    username: ['',Validators.required],
    pazzword: ['',[Validators.required,Validators.pattern('[A-Za-z0-9]{6,}')]],
    confirmPazzword: ['',[Validators.required,paramPasswordValidator(this.passwordPattern)]]
  },{validator : passwordMatchingValidator})

  ngOnInit(): void {
  }

  @Input() appName: string;
  
  public get username(){
    return this.newUserForm.get('username');
  }

  public get pazzword(){
    return this.newUserForm.get('pazzword');
  }

  public get confirmPazzword(){
    return this.newUserForm.get('confirmPazzword');
  }
  
  register(){
    alert("Hi " + this.username.value + ". You are Succesfully registered!");
  }

  @Output() existingUser = new EventEmitter();
  setExistingUser(){
    this.existingUser.emit(true);
  }
}
