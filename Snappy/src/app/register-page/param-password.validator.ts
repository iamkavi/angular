import { AbstractControl } from "@angular/forms";

export function paramPasswordValidator(passwordPattern : RegExp){
    return (control: AbstractControl) => {  
        const valid = control.value.match(passwordPattern);
        return control.value && !valid?{'passWordLength': {value: control.value}}:null;
    }; 
}