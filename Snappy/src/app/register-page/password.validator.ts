import { AbstractControl } from "@angular/forms";

export function passwordValidator(control : AbstractControl){
    var passwordPattern='[A-Za-z0-9]{6,}';
    const valid = control.value.match(passwordPattern);
    return !valid?{'passWordLength': {value: control.value}}:null;
}